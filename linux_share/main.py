"""
DX_All
================

Demo front-end software for DX_All device
"""
# Author: Stefano Latini & Waldemar Kolodziejczyk

# Kivy imports
from kivy.app import App
from kivy.config import Config
from kivy.clock import Clock
from kivy.graphics import Color, Rectangle
from kivy.lang import Builder
from kivy.core.window import Window
from kivy.uix.vkeyboard import VKeyboard
from kivy.properties import ObjectProperty
from kivy.uix.button import Button
from functools import partial
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.scrollview import ScrollView
from kivy.uix.boxlayout import BoxLayout
from kivy.config import Config
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy import require
from kivy.garden.graph import MeshLinePlot
from kivy.properties import StringProperty
from kivy.properties import ListProperty
from kivy.animation import Animation
from kivy.uix.textinput import TextInput
from random import randint
from math import sin
import os, sys

# Native imports
import random
import threading

# This example uses features introduced in Kivy 1.8.0, namely being able
# to load custom json files from the app folder.
require("1.8.0")


"""
#ListViewItem markup code
<ListviewItem>:
    canvas.before:
        Color:
            rgba:1,1,1,1
        Rectangle:
            pos:self.pos
            size:self.size
    rows:1
    cols:2
    size_hint_y:None
    spacing:50
    Label:
        id:red_label
        size_hint_x:None
        width:150
        text:"1"
        canvas.before:
            Rectangle:
                pos: self.pos
                size: self.size
                source: "label_bckg.png"
    Label:
        height:red_label.height
        font_size:40
        size_hint: (1,None)
        multiline:False
        text:"342352266"
        color: 0,0,0,1
        canvas.before:
            Color:
                rgba: 1,1,1,1
            Rectangle:
                pos: self.pos
                size: self.size_hint
"""

Builder.load_string('''    

# Graph screen markup code
<GraphScreen>:
    BoxLayout:
    
        orientation: "vertical"
    	
    	canvas:
            Color:
                rgba: 0.18, 0.2115, 0.45, 1
            Rectangle:
                size: self.size
        Label:
            text:"Voltage Graph"
            color: 1,1,1,1
            font_size:50
        BoxLayout:
            size_hint: [1, .8]
            Graph:
                xmin:-0
                xmax:100
                ymin:-1
                ymax:1
                y_ticks_major:1
                id: graph
                xlabel: ""
                ylabel: "Level"
        GridLayout:
            cols:3
            rows:1
            spacing:15
            padding:5
            size_hint: [1, .2]
            orientation: "horizontal"
            Button:
                text: "TEST"
                background_down:"uiux/btn_bckg_p.png"
                background_normal:"uiux/btn_bckg.png"
            Button:
                canvas.before:
                    Color:
                        rgba: (0,0,0,1) if self.state=='normal' else (0,.7,.7,1)  # visual feedback of press
                    RoundedRectangle:
                        pos: self.pos
                        size: self.size
                        radius: [10,10]
                text: "< Back "
                bold: True
                on_press:root.menu()
            Button:
                id:start_button
                canvas.before:
                    Color:
                        rgba: (0,0,0,0) if self.state=='normal' else (0,.7,.7,1)  # visual feedback of press
                    RoundedRectangle:
                        pos: self.pos
                        size: self.size
                        radius: [10,10]
                text: "Start"
                bold: True
                on_press:root.plot()
    

# Barcode scroll screen markup code
<ScrollScreen>:
    padding:15
    canvas.before:
        Color:
            rgba:0.8,0.8,0.8,1
        Rectangle:
            pos:self.pos
            size:self.size
    BoxLayout:
        canvas:
            Color:
                rgba: 0.18, 0.2115, 0.45, 1
            Rectangle:
                size: self.size
        orientation:"vertical"
        size_hint_y:1
        GridLayout:
            pos_hint:{"x":0.1,"y":0}
            size_hint:0.8,0.5
            height:30
            padding:20
            spacing:10
            cols:1
            rows:2
            Label:
                text:"Run Test"
                font_size:50
                halign: 'center'
                valign: 'middle'
            GridLayout:
                spacing:10
                cols:1
                rows:2 
                Button:
                    background_down:"uiux/btn_bckg_p.png"
                    background_normal:"uiux/btn_bckg.png"
                    font_size:20
                    text:"< Back"
                    on_press:root.prev()
                Button:
                    background_down:"uiux/btn_bckg_p.png"
                    background_normal:"uiux/btn_bckg.png"
                    font_size:20
                    text:"Voltage"
                    on_press:root.graph()
            
        ScrollView:
            bar_width:0
            pos_hint:{"x":0.1,"y":0.5}
            size_hint:0.8,0.5
            GridLayout:
                id:listview_container
                height:self.minimum_height
                cols:1
                spacing:10
                size_hint_y:None
                GridLayout:
                    canvas.before:
                        Color:
                            rgba:1,1,1,1
                        Rectangle:
                            pos:self.pos
                            size:self.size
                    rows:1
                    cols:2
                    size_hint_y:None
                    soacing:50
                    Label:
                        id:red_label
                        size_hint_x:None
                        width:150
                        text:"1"
                        canvas.before:
                            Rectangle:
                                pos: self.pos
                                size: self.size
                                source: "uiux/label_bckg.png"
                    Label:
                        height:red_label.height
                        font_size:40
                        size_hint: (1,None)
                        multiline:False
                        text:"342352266"
                        color: 0,0,0,1
                        canvas.before:
                            Color:
                                rgba: 1,1,1,1
                            Rectangle:
                                pos: self.pos
                                size: self.size
                GridLayout:
                    canvas.before:
                        Color:
                            rgba:1,1,1,1
                        Rectangle:
                            pos:self.pos
                            size:self.size
                    rows:1
                    cols:2
                    size_hint_y:None
                    soacing:50
                    Label:
                        id:red_label
                        size_hint_x:None
                        width:150
                        text:"1"
                        canvas.before:
                            Rectangle:
                                pos: self.pos
                                size: self.size
                                source: "uiux/label_bckg.png"
                    Label:
                        height:red_label.height
                        font_size:40
                        size_hint: (1,None)
                        multiline:False
                        text:"342352266"
                        color: 0,0,0,1
                        canvas.before:
                            Color:
                                rgba: 1,1,1,1
                            Rectangle:
                                pos: self.pos
                                size: self.size
                GridLayout:
                    canvas.before:
                        Color:
                            rgba:1,1,1,1
                        Rectangle:
                            pos:self.pos
                            size:self.size
                    rows:1
                    cols:2
                    size_hint_y:None
                    soacing:50
                    Label:
                        id:red_label
                        size_hint_x:None
                        width:150
                        text:"1"
                        canvas.before:
                            Rectangle:
                                pos: self.pos
                                size: self.size
                                source: "uiux/label_bckg.png"
                    Label:
                        height:red_label.height
                        font_size:40
                        size_hint: (1,None)
                        multiline:False
                        text:"342352266"
                        color: 0,0,0,1
                        canvas.before:
                            Color:
                                rgba: 1,1,1,1
                            Rectangle:
                                pos: self.pos
                                size: self.size
                GridLayout:
                    canvas.before:
                        Color:
                            rgba:1,1,1,1
                        Rectangle:
                            pos:self.pos
                            size:self.size
                    rows:1
                    cols:2
                    size_hint_y:None
                    soacing:50
                    Label:
                        id:red_label
                        size_hint_x:None
                        width:150
                        text:"1"
                        canvas.before:
                            Rectangle:
                                pos: self.pos
                                size: self.size
                                source: "uiux/label_bckg.png"
                    Label:
                        height:red_label.height
                        font_size:40
                        size_hint: (1,None)
                        multiline:False
                        text:"342352266"
                        color: 0,0,0,1
                        canvas.before:
                            Color:
                                rgba: 1,1,1,1
                            Rectangle:
                                pos: self.pos
                                size: self.size
                GridLayout:
                    canvas.before:
                        Color:
                            rgba:1,1,1,1
                        Rectangle:
                            pos:self.pos
                            size:self.size
                    rows:1
                    cols:2
                    size_hint_y:None
                    soacing:50
                    Label:
                        id:red_label
                        size_hint_x:None
                        width:150
                        text:"1"
                        canvas.before:
                            Rectangle:
                                pos: self.pos
                                size: self.size
                                source: "uiux/label_bckg.png"
                    Label:
                        height:red_label.height
                        font_size:40
                        size_hint: (1,None)
                        multiline:False
                        text:"342352266"
                        color: 0,0,0,1
                        canvas.before:
                            Color:
                                rgba: 1,1,1,1
                            Rectangle:
                                pos: self.pos
                                size: self.size
                GridLayout:
                    canvas.before:
                        Color:
                            rgba:1,1,1,1
                        Rectangle:
                            pos:self.pos
                            size:self.size
                    rows:1
                    cols:2
                    size_hint_y:None
                    soacing:50
                    Label:
                        id:red_label
                        size_hint_x:None
                        width:150
                        text:"1"
                        canvas.before:
                            Rectangle:
                                pos: self.pos
                                size: self.size
                                source: "uiux/label_bckg.png"
                    Label:
                        height:red_label.height
                        font_size:40
                        size_hint: (1,None)
                        multiline:False
                        text:"342352266"
                        color: 0,0,0,1
                        canvas.before:
                            Color:
                                rgba: 1,1,1,1
                            Rectangle:
                                pos: self.pos
                                size: self.size
                GridLayout:
                    canvas.before:
                        Color:
                            rgba:1,1,1,1
                        Rectangle:
                            pos:self.pos
                            size:self.size
                    rows:1
                    cols:2
                    size_hint_y:None
                    soacing:50
                    Label:
                        id:red_label
                        size_hint_x:None
                        width:150
                        text:"1"
                        canvas.before:
                            Rectangle:
                                pos: self.pos
                                size: self.size
                                source: "uiux/label_bckg.png"
                    Label:
                        height:red_label.height
                        font_size:40
                        size_hint: (1,None)
                        multiline:False
                        text:"342352266"
                        color: 0,0,0,1
                        canvas.before:
                            Color:
                                rgba: 1,1,1,1
                            Rectangle:
                                pos: self.pos
                                size: self.size

                
# Main login screen markup code      
<MainScreen>:
    center_label: center_label
    BoxLayout:
        id:main_boxlayout
        canvas:
            Color:  
                rgba: 0.18, 0.2115, 0.45, 1
            Rectangle:
                size: self.size
        orientation: "vertical"
        size_hint: 1,1
        pos_hint: {"x": 0, "y": 0}
        padding: "5sp"
        spacing: "5sp"
        Label:
            text_size: self.size
            canvas:
            markup: True
            text_size: self.size
            font_size:50
            halign: 'center'
            valign: 'middle'     
            text: "DxALL"
            size_hint_y: 0.2
        Widget:
            size_hint_y: 0.1
        BoxLayout:
            spacing:20
            orientation: "vertical"
            size_hint: 0.8,0.5
            pos_hint:{"x":0.1, "y":0}
            GridLayout:
                spacing:30
                cols:1
                rows:3
                size_hint_y: 0.2
                GridLayout:
                    cols:2
                    spacing:0
                    Label:
                        id:user_label
                        size_hint_x:None
                        width:100
                        canvas.before:
                            Rectangle:
                                pos: self.pos
                                size: self.size
                                source: "uiux/label_lign_bckg.png"
                    TextInput:
                        id:username
                        height:user_label.height
                        font_size: self.height - 40
                        size_hint: (.2,None)
                        multiline:False
                        text:"user"
                GridLayout:
                    cols:2
                    spacing:0
                    Label:
                        id:password_label
                        size_hint_x:None
                        width:100
                        canvas.after:
                            Rectangle:
                                pos: self.pos
                                size: self.size
                                source: "uiux/label_lign_bckg.png"
                    TextInput:
                        id:password
                        height:password_label.height
                        font_size: self.height - 40
                        password:True
                        size_hint: (.2,None)
                        multiline:False
                        text:"password"
                BoxLayout:
                    spacing:20
                    orientation: "horizontal"
                    size_hint_y: 0.4
                    Button:
                        background_down:"uiux/btn_bckg_p.png"
                        background_normal:"uiux/btn_bckg.png"
                    	font_size:20
                        text: "Exit"
                        on_release: exit()
                    Button:
                        background_down:"uiux/btn_bckg_p.png"
                        background_normal:"uiux/btn_bckg.png"
                    	font_size:20
                        text: "Login"
                        on_release: root.next()
        Label:
            text:"[b]CELLMIC[/b]"
            color: 1,0,0,1
            id: center_label
            markup: True
            text_size: self.size
            font_size:70
            size_hint_y: 0.3
            halign: 'center'
            valign: 'middle'

''')

#ListView items defs

class ListViewItem(GridLayout):
    code = StringProperty()
    def __init__(self, **kwargs):
        super(ListViewItem,self).__init__(**kwargs)

        with self.canvas.before:
            Color(1, 1, 1, 1)  # set the colour to red
            self.rect = Rectangle(pos=self.pos,size=self.size)

        layout = GridLayout(rows=1,cols=2,size_hint_y=None,spacing=50)
        label = Label(id="red_label",size_hint_x=None,width=150)
        label.canvas.before.add(Rectangle(pos=self.pos,size=self.size,source="uiux/label_bckg.png"))
        label2 = Label(height=label.height,font_size=40,size_hint=(1,None),multiline=False,text=self.code,color=(0,0,0,1))
        label2.canvas.before.add(Color(1,1,1,1))
        label2.canvas.before.add(Rectangle(pos=self.pos,size=self.size))
        layout.add_widget(label)
        layout.add_widget(label2)
        self.add_widget(layout)
        
#Screen Classes defs

class MainScreen(Screen):
    """
    Present the option to change keyboard mode and warn of system-wide
    consequences.
    """
    center_label = ObjectProperty()
    keyboard_mode = ""

    def on_user_focus(self, instance, value):
        if value:
            Animation(y=100, t='in_sine', d=.2).start(self)
            print('User focused', instance)
        else:
            Animation(y=0, t='out_sine', d=.2).start(self)
            print('User defocused', instance)

    def on_password_focus(self, instance, value):
        if value:
            Animation(y=300, t='in_sine', d=.2).start(self)
            print('User focused', instance)
        else:
            Animation(y=0, t='out_sine', d=.2).start(self)
            print('User defocused', instance)

    def __init__(self, **kwargs):
        print("we're set the main screen..\n")
        super(MainScreen, self).__init__(**kwargs)
        self._keyboard = None
        self.set_keyboard()
        print(self._keyboard)
        print(self._keyboard.docked)
        username = TextInput()
        username = self.ids.username
        username.bind(focus=self.on_user_focus)
        password = TextInput()
        password = self.ids.password
        password.bind(focus=self.on_password_focus)
        

    def set_keyboard(self):
        kb = Window.request_keyboard(
            self._keyboard_close,self)
        if kb.widget:
            print("we're setting the keyboard..\n")
            self._keyboard = kb.widget
            #self._keyboard.layout = "querty"
            key_margin = ListProperty([])
            bck_color = ListProperty([])
            margin_hint = ListProperty([])
            bck_color = [1.2, 1.2, 1.2, 1]
            key_margin = [2,2,2,2]
            margin_hint = [.01,.01,.01,.01]

            self._keyboard.layout = 'keyboard.json'
            self._keyboard.key_margin = key_margin
            self._keyboard.margin_hint = margin_hint
        else:
            print("we're using the same old keyboard..\n")
            self._keyboard = kb

        self._keyboard.bind(on_key_down=self.key_down,
                            on_key_up=self.key_up)


    def _keyboard_close(self, *args):
        if self._keyboard:
            self._keyboard.unbind(on_key_down=self.key_down)
            self._keyboard.unbind(on_key_up=self.key_up)
            self._keyboard = None

    def key_down(self, keyboard, keycode, text, modifiers):
        """ The callback function that catches keyboard events. """
        #self.displayLabel.text = u"Key pressed - {0}".format(text)

    # def key_up(self, keyboard, keycode):
    def key_up(self, keyboard, keycode, *args):
        """ The callback function that catches keyboard events. """
        # system keyboard keycode: (122, 'z')
        # dock keyboard keycode: 'z'
        if isinstance(keycode, tuple):
            keycode = keycode[1]
        #self.displayLabel.text += u" (up {0})".format(keycode)

    def next(self):
        """ Continue to the main screen """
        self.manager.current = "scroll"


class ScrollScreen(Screen):
    def __init__(self, **kwargs):
        super(ScrollScreen, self).__init__(**kwargs)
        self.first_thread()
        #self.ids.listview_container.add_widget(ListViewItem(code="PIPPOH"))
        

    def prev(self):
        """ Continue to the main screen """
        self.manager.current = "main"
        
    def graph(self):
        self.manager.current = "graph2"

    
    def first_thread(self):
        threading.Thread(target = self.read_barcode).start()

    def read_barcode(self):
        print("reading..")
        print("captured 'A'")
        self.ids.listview_container.add_widget(ListViewItem(code="Pippoh"))
    


class GraphScreen(Screen):
    def __init__(self,**kwargs):
        super(GraphScreen, self).__init__(**kwargs)

    def plot(self):

        self.ids.start_button.text = "Stop"
        #Clock.schedule_interval(self.plot_random_value, 0.001)
        self.plot_random_value()
    
    def menu(self):
        #Clock.unschedule(self.plot_random_value)
        for plot in self.ids.graph.plots:
            self.ids.graph.remove_plot(plot)
        self.manager.current = "scroll"
    
    def plot_random_value(self):
        global levels
        plot = MeshLinePlot(line_width=5,color=[1, 1, 1, 1])
        plot.points = [(x, sin(x / random.uniform(1,2))) for x in range(0, 101)]
        self.ids.graph.add_plot(plot)
        #levels = []
        #for count in range(0,100):
            #mx = randint(0, 9)
            #levels.append(mx)
        #self.plot.points = [(i, j/5) for i, j in enumerate(levels)]
        

   


class DXAll(App):
    Config.set('kivy','keyboard_mode','systemanddock')
    Config.write()
    
    sm = ObjectProperty(None)  # The root screen manager
    def build(self):

        self.sm = ScreenManager()
        self.sm.add_widget(MainScreen(name="main"))
        self.sm.add_widget(GraphScreen(name="graph2"))
        self.sm.add_widget(ScrollScreen(name="scroll"))
        self.sm.current = "main"
        return self.sm
        
if __name__ == "__main__":
    levels = []  # store levels of microphone
    DXAll().run()
